function showServices(baseUrl) {
    $.getJSON(baseUrl + '/plugins/servlet/jsonService/defaultService/getDetailServices?callback=?', function (json) {
        var strTable = "<table width='100%'>"+
                "<tr><td align='center'><div class='menuheading'>Available Services</div></td></tr><tr><td>"+
                "<table width='100%' border=0><tr bgcolor='#f9f9f9'><td valign='middle'><table border='0' width='100%'>";
        for (service in json) {
            var methodsName = json[service];
            strTable = strTable + "<tr><td valign='top' width='20%'><b>" + service + "</b></td><td width='80%'><table border='0' width='100%'>"
            for (method in methodsName) {
                strTable = strTable + "<tr><td><img src='/images/icons/bullet_creme.gif' height='8' width='8'></img><span style='padding: 3px'>" + methodsName[method] + "</span></td></tr>";
            }
            strTable = strTable + "</table></td></tr>";
        }
        strTable = strTable + "</table></td></tr></table></td></tr></tbody></table>";
        $("#services").text("");
        $("#services").append(strTable);
    });
}