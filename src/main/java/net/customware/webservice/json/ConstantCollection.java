package net.customware.webservice.json;

import com.atlassian.confluence.plugin.descriptor.rpc.XmlRpcModuleDescriptor;

import java.util.Map;
import java.util.HashMap;

public class ConstantCollection {
	public static final String PATH_INFO = "/jsonService/";
    public static final String SLASH = "/";

    public static final int FLAG_CHANGE_TO_MACRO = 0;
    public static final int FLAG_CHANGE_TO_ESCAPE = 1;

    public final static int SEQ_METHOD = 1;
	public final static int SEQ_SERVICE = 0;

    public final static int PARAMETER_INDEX = 0;

    public final static String START_CHARACTER_JSON = "{";
    public final static String END_CHARACTER_JSON = "}";

    public final static Class JSON_DESCRIPTOR = XmlRpcModuleDescriptor.class;

    public final static String PROXIED_LOGIN = "login";
    public final static String PROXIED_LOGOUT = "logout";

    public final static String DEFAULT_SERVICE = "default";
    public final static String URL_PARAMETERS = "parameters";
    public final static String PARAMETERS_SEPARATOR = ";";

    public final static int XMLRPC_SERVICE = 0;
    public final static int SOAP_SERVICE = 1;
    
    public static Map<String,String> HTML_ESCAPE_CHARACTER = new HashMap();
    static
    {
        HTML_ESCAPE_CHARACTER.put("%20", " ");
        HTML_ESCAPE_CHARACTER.put("%22", "\"");
        HTML_ESCAPE_CHARACTER.put("%24", "$");
        HTML_ESCAPE_CHARACTER.put("%3B", ";");
        HTML_ESCAPE_CHARACTER.put("%7B", "{");
        HTML_ESCAPE_CHARACTER.put("%2D", "}");
    }
    public static final String MACRO_CHAR_OPEN = "\\{";
    public static final String MACRO_CHAR_OPEN_ESCAPE = "%7B";

    public static final String MACRO_CHAR_CLOSE = "\\}";
    public static final String MACRO_CHAR_CLOSE_ESCAPE = "%2D";



    public static final String PREPENDED_LABEL = "Confluence";
    public static final String LABEL_NAME = "callback";

    public static final int JSON_PROPERTY_ID = 10784;
    public static final String JSON_PROPERTY_NAME = "serviceType";
    public static final String JSON_PROPERTY_KEY = "net.customware.webservice.json";
    public static final int JSON_DEFAULT_SERVICE = XMLRPC_SERVICE;

    public static final String EXCEPTION_SERVICE_UNAVAILABLE= "Specified service is not available. Please refer to <Confluence Path>/admin/JsonConfigurationPage.action";
    public static final String EXCEPTION_METHOD_UNAVAILABLE= "Specified method is not available. Please refer to <Confluence Path>/admin/JsonConfigurationPage.action"; 
}
