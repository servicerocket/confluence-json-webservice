package net.customware.webservice.json.util;

import bucket.container.ContainerManager;

import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.customware.webservice.json.service.loader.ServiceLoader;
import net.customware.webservice.json.service.ConfluenceJsonService;
import net.customware.webservice.json.ConstantCollection;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Mar 23, 2009
 * Time: 10:41:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class ServiceUtil {
    private Logger logger = LoggerFactory.getLogger(ServiceUtil.class);
    private HttpJsonUtil httpJsonUtil = new HttpJsonUtil();

    public String invokeAllServices(Object service, Map<Method, Object[]> pairMethodParams) throws Exception{
        Iterator<Map.Entry<Method, Object[]>> pairMethodParamsIterator = pairMethodParams.entrySet().iterator();
        ConfluenceJsonService executor = (ConfluenceJsonService) ContainerManager.getComponent("jsonServiceDelegator");
        List results = new ArrayList();
        while (pairMethodParamsIterator.hasNext()) {
            Map.Entry<Method, Object[]> methodParams = pairMethodParamsIterator.next();
            Method methodToExecute = methodParams.getKey();
            Object[] params = methodParams.getValue();
            try {
                Object result = executor.executeMethod(service, methodToExecute, params, methodToExecute.getName());
                if (result != null)
                    results.add(result);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (results.size() == 1) {
            return JsonUtil.convertObjectToJson(this.filterConfluenceMacroInObject(results.get(0), ConstantCollection.FLAG_CHANGE_TO_ESCAPE));
        }
//        if (results.size() == 0)
//            throw new JsonMethodUnavailableException();
        return JsonUtil.convertObjectToJson(results);
    }

    public Object filterConfluenceMacroInObject(Object castedResult, int flag) throws Exception {
        Class castedClass = castedResult.getClass();
        Field[] fields = castedClass.getDeclaredFields();
        if(castedResult instanceof Hashtable)
        {
            Hashtable fieldsInHashtable = (Hashtable) castedResult;
            return this.escapeObjectContainsMacro(fieldsInHashtable);
        }
        for (int i = 0; i < fields.length; i++) {
            String propertyName = fields[i].getName();
            Method getterMethod = getMethodOfProperty(castedClass, propertyName, "get");
            if (getterMethod == null) continue;
            Object fieldObject = getterMethod.invoke(castedResult);
            if (fieldObject == null) continue;
            if (fieldObject instanceof String) {
                if(ConstantCollection.FLAG_CHANGE_TO_MACRO == flag)
                    fieldObject = changeEscapeCharacterToMacro(fieldObject.toString());
                else
                    fieldObject = changeMacroToEscapeCharacter(fieldObject.toString());
                Method setterMethod = getMethodOfProperty(castedClass, propertyName, "set");
                setterMethod.invoke(castedResult, fieldObject);
            }
        }
        return castedResult;
    }
    public Hashtable escapeObjectContainsMacro(Hashtable fields)
    {
        Enumeration keyFieldsCollection = fields.keys();
        while(keyFieldsCollection.hasMoreElements())
        {
            Object key = keyFieldsCollection.nextElement();
            if(fields.get(key) instanceof String)
            {
                String fieldValueToChange = fields.get(key).toString();
                String changedFieldValue = this.changeMacroToEscapeCharacter(fieldValueToChange);
                fields.put(key, changedFieldValue);
            }
        }
        return fields;
    }

    public String changeMacroToEscapeCharacter(String fieldValue) {
        fieldValue = fieldValue.replaceAll(ConstantCollection.MACRO_CHAR_OPEN, ConstantCollection.MACRO_CHAR_OPEN_ESCAPE);
        fieldValue = fieldValue.replaceAll(ConstantCollection.MACRO_CHAR_CLOSE, ConstantCollection.MACRO_CHAR_CLOSE_ESCAPE);

        return fieldValue;
    }
    public String changeEscapeCharacterToMacro(String fieldValue) {
        if (fieldValue.contains(ConstantCollection.MACRO_CHAR_OPEN_ESCAPE)) {
            fieldValue = fieldValue.replaceAll(ConstantCollection.MACRO_CHAR_OPEN_ESCAPE, ConstantCollection.MACRO_CHAR_OPEN);
            fieldValue = fieldValue.replaceAll(ConstantCollection.MACRO_CHAR_CLOSE_ESCAPE, ConstantCollection.MACRO_CHAR_CLOSE);
        }
        return fieldValue;
    }

    public Map<String, List> getAvailableServices() {
        ServiceLoader serviceLoader = (ServiceLoader) ContainerManager.getComponent("servicesLoader");
        Map<String, Object> services = serviceLoader.getServices();
        Map<String, List> availableServices = new HashMap<String, List>();
        Iterator<Map.Entry<String, Object>> serviceIterator = services.entrySet().iterator();

        while (serviceIterator.hasNext()) {
            Map.Entry<String, Object> service = serviceIterator.next();
            String serviceKey = service.getKey();
            List exposibleMethods = new ArrayList<Method>();
            Object serviceObject = service.getValue();
            Class[] serviceInterfaces = serviceObject.getClass().getInterfaces();

            for (int i = 0; i < serviceInterfaces.length; i++) {
                Class serviceInterface = serviceInterfaces[i];
                Method[] methods = serviceInterface.getMethods();
                for (int j = 0; j < methods.length; j++) {
                    exposibleMethods.add(methods[j]);
                }
            }
            availableServices.put(serviceKey, exposibleMethods);
        }
        return availableServices;
    }
// Still a lot of consideration to think, rectify this for the next steps

    // ========================================================================================

    public Method getMethodOfProperty(Class className, String propertyName, String prefix) {
        Method[] methods = className.getMethods();
        for (Method method : methods) {
            String methodName = method.getName();
            methodName = methodName.toLowerCase();
            if (methodName.equals((prefix + propertyName).toLowerCase()))
                return method;
        }
        return null;
    }

    public Object getService(String serviceName) {
        if (serviceName.equals("defaultService")) {
            Object service = ContainerManager.getComponent(serviceName);
            return service; 
        }
        ServiceLoader serviceLoader = (ServiceLoader) ContainerManager.getComponent("servicesLoader");
        return serviceLoader.getService(serviceName);
    }
//==================================================================================================

    public Method getMethod(Object service, String methodName, Class[] parameters) throws NoSuchMethodException {
        return service.getClass().getMethod(methodName, parameters);
    }

    // currently does not support methods which have the same name
    public Map<Method, Object[]> matchMethod(Object service, String methodName, List<String> parametersValue) {
        Method[] methods = service.getClass().getMethods();
        Map<Method, Object[]> methodnParameters = new HashMap<Method, Object[]>();
        for (int i = 0; i < methods.length; i++) {
            Method method = methods[i];
            String methodNameFromClass = method.getName();
            if (methodName.equals(methodNameFromClass)) {
                Object[] params = localizeParameter(method, parametersValue);
                if (params != null) {
                    methodnParameters.put(method, params);
                }
            }
        }
        return methodnParameters;
    }

    // To get parameter as the method needed. Converting all the parameter so that it is matched with
    // method's parameter types
    public Object[] localizeParameter(Method method, List<String> parametersValue) {
        Class<?>[] originalParametersType = method.getParameterTypes();

// if the number of parameters are not matched, it should give you an exception. I havent created that
// exception class

        if (originalParametersType.length != parametersValue.size())
            return null;

        int counter = 0;

        Object[] localizedParameter = new Object[parametersValue.size()];
        for (int i = 0; i < parametersValue.size(); i++) {
            String parameterContent = parametersValue.get(i);
//            parameterContent = httpJsonUtil.changeHTMLEscapeCharacter(parameterContent);
            Class<?> parameterType = originalParametersType[counter];
            try {
                localizedParameter[counter] = JsonUtil.convertJsonStringToObject(parameterContent, parameterType);
            }
            catch (Exception e) {

            }
            counter++;
        }
        return localizedParameter;
    }
}