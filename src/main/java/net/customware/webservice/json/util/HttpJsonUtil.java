package net.customware.webservice.json.util;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.customware.webservice.json.ConstantCollection;
import com.atlassian.confluence.util.GeneralUtil;

/**
 * Utility class to handle anything related to http.
 */
public class HttpJsonUtil {
	Logger logger = LoggerFactory.getLogger(HttpJsonUtil.class);

    /**
     * Method to decode the escaped character of the url address.
     */
    public String changeHTMLEscapeCharacter(String url)
    {
        url = GeneralUtil.urlDecode(url);
        return url;
    }
    /**
     * Method to write data to client (browser). It will prepend the label. For certain library, such as
     * jquery needs callback to process the data further.
     */
    public void sendToClient(HttpServletResponse response, HttpServletRequest request, String output)
    {
        ServiceUtil serviceUtil = new ServiceUtil();
        String prependedLabel = request.getParameter(ConstantCollection.LABEL_NAME);
        if(prependedLabel == null)
            prependedLabel = ConstantCollection.PREPENDED_LABEL;
        if(!prependedLabel.equals("noLabel")){
            output = prependedLabel +"("+output+")";
            output = serviceUtil.changeEscapeCharacterToMacro(output);
        }
        
        byte[] clientData = output.getBytes();
        try
        {
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(clientData);
            outputStream.flush();
            outputStream.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    /**
     * Fetch the JSON parameters for the service that client wants to call. By default the plugin will use
     * separator ';' (without single quote).
     */
    public List<String> getParameters(HttpServletRequest request)
    {
        List outputParameter = new ArrayList();
        String queryString = request.getParameter(ConstantCollection.URL_PARAMETERS);
        if(queryString != null){
            String[] pairParameterValues = queryString.split(ConstantCollection.PARAMETERS_SEPARATOR);
            for (int i = 0; i < pairParameterValues.length; i++) {
                String value = pairParameterValues[i];//.replaceFirst(".+=", "");
                value = value.trim();
                String regex = "'\\{(.*)\\}'";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(value);
                value = matcher.replaceAll("'"+ConstantCollection.MACRO_CHAR_OPEN_ESCAPE+"$1"+ConstantCollection.MACRO_CHAR_CLOSE_ESCAPE+"'");
                if (!value.equals(""))
                    outputParameter.add(value);
            }
        }
        
        return outputParameter;
    }
    /**
     * Get the service name that client wants to call
     */
    public String getServiceName(HttpServletRequest request)
	{
		String pathInfo = this.fetchServiceMethodName(request);
		return pathInfo.split(ConstantCollection.SLASH)[ConstantCollection.SEQ_SERVICE];
	}
    /**
     * Get the method name that client wants to call
     */
    public String getMethodName(HttpServletRequest request)
	{
		String methodName = this.fetchServiceMethodName(request);
		return methodName.split(ConstantCollection.SLASH)[ConstantCollection.SEQ_METHOD];
	}
    /**
     * Get the string which contains serviceName/methodName. So, it will be used by getMethodName and
     * getServiceName.
     */
    public String fetchServiceMethodName(HttpServletRequest request)
	{
		String pathInfo = request.getPathInfo();
		pathInfo = pathInfo.replaceFirst(ConstantCollection.PATH_INFO, "");
        if(pathInfo.indexOf(ConstantCollection.SLASH) == 0)
            pathInfo = pathInfo.replaceFirst(ConstantCollection.SLASH, "");
        return pathInfo;
	}
	
	
}
