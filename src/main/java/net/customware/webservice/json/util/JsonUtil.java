package net.customware.webservice.json.util;

import net.sf.json.JSONObject;
import net.sf.json.JSONArray;
import net.customware.webservice.json.ConstantCollection;

import java.util.Vector;
import java.util.List;
import java.util.Hashtable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.rpc.xmlrpc.Translator;
import org.apache.batik.dom.util.HashTable;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Mar 23, 2009
 * Time: 9:53:58 AM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Utility class to convert Java object to JSON string and vice versa
 */
public class JsonUtil {
    /**
     * Convert Java object to String. Before going to this method, all the characters which cause the problem
     * should be escaped so it wont crash this method and no ruin the JSON rule.
     */
    public static String convertObjectToJson(Object javaObject) {
        if (javaObject instanceof String) {
            return "\"" + javaObject.toString() + "\"";
        }
        if (javaObject.getClass().isArray() || javaObject instanceof Vector || javaObject instanceof List) {
            JSONArray arrayOfResult = JSONArray.fromObject(javaObject);
            return arrayOfResult.toString();
        }
        return JSONObject.fromObject(javaObject).toString();
    }

    /**
     * Convert JSON string to Java object
     */
    public static Object convertJsonStringToObject(String jsonString, Class objectType) {
        ServiceUtil serviceUtil = new ServiceUtil();
        Object castedResult = null;
        try {
            JSONObject jsonObject = JSONObject.fromObject(jsonString);
            castedResult = JSONObject.toBean(jsonObject, objectType);
            castedResult = serviceUtil.filterConfluenceMacroInObject(castedResult, ConstantCollection.FLAG_CHANGE_TO_MACRO);
        }
        catch (Exception e) {
            if (objectType.getName().equals(Integer.class.getName())) {
                castedResult = new Integer(jsonString);
            } else if (objectType.getName().equals(Long.class.getName())) {
                castedResult = new Long(jsonString);
            } else if (objectType.getName().equals(Double.class.getName())) {
                castedResult = new Double(jsonString);
            } else if (objectType.getName().equals(int.class.getName())) {
                castedResult = new Integer(jsonString).intValue();
            } else if(objectType.getName().equals(long.class.getName())){
                castedResult = new Long(jsonString).longValue();
            }
            else {
                castedResult = jsonString;
            }
        }
        return castedResult;
    }


}
