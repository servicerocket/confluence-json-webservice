package net.customware.webservice.json.util;

import bucket.user.persistence.dao.hibernate.BucketPropertySetDAO;
import bucket.user.propertyset.BucketPropertySetItem;
import bucket.container.ContainerManager;
import com.opensymphony.module.propertyset.hibernate.PropertySetItem;
import com.opensymphony.module.propertyset.hibernate.HibernatePropertySetDAO;
import net.customware.webservice.json.ConstantCollection;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: 25/04/2009
 * Time: 12:56:47
 * To change this template use File | Settings | File Templates.
 */
/**
 * Utility class which deals with database. This plugin stores the configuration of what type of service currently
 * being loaded, either SOAP or XMLRPC.
 */
public class ConfigurationUtil {
    private static final HibernatePropertySetDAO propertySetDAO = (HibernatePropertySetDAO)ContainerManager.getInstance().getContainerContext().getComponent("propertySetDao");

    /**
     * Stores configuration to database, not UPDATING;
     */
    public static int saveConfiguration(int value)
    {
        BucketPropertySetItem property = getJsonProperty();
        property.setIntVal(value);
        propertySetDAO.setImpl(property, false);
        return property.getIntVal();
    }
    /**
     * Get the Property for JSON's configuration
     */
    public static BucketPropertySetItem getJsonProperty()
    {
        BucketPropertySetItem property = new BucketPropertySetItem();
        property.setEntityId(ConstantCollection.JSON_PROPERTY_ID);
        property.setEntityName(ConstantCollection.JSON_PROPERTY_NAME);
        property.setKey(ConstantCollection.JSON_PROPERTY_KEY);
        return property;
    }
    /**
     * Updating the configuration (service's flag)
     */
    public static void updateConfiguration(int value)
    {
        PropertySetItem property = getJsonProperty();
        property.setIntVal(value);
        propertySetDAO.setImpl(property, true);
    }
    /**
     * Fetching the flag (flag for SOAP is 1 and XMLRPC is 0) in regard of what services being loaded.
     */
    public static int getConfiguration()
    {
        PropertySetItem property = getJsonProperty();
        property = propertySetDAO.findByKey(property.getEntityName(), property.getEntityId(), property.getKey());
        if(property == null){
            return ConfigurationUtil.saveConfiguration(ConstantCollection.JSON_DEFAULT_SERVICE);
        }
        return property.getIntVal();
    }
}
