package net.customware.webservice.json.service;


import java.lang.reflect.Method;

import com.atlassian.confluence.rpc.RemoteException;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Apr 5, 2009
 * Time: 10:14:46 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ConfluenceJsonService {
    public Object executeMethod(Object service, Method method, Object[] params, String methodName) throws RemoteException;
}
