package net.customware.webservice.json.service;

import net.customware.webservice.json.service.loader.ServiceLoader;

import java.util.*;
import java.lang.reflect.Method;

import bucket.container.ContainerManager;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Apr 17, 2009
 * Time: 10:20:11 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * This class is internal json service to provide a list of available services available on Confluence.
 */
public class JsonServiceImpl implements JsonService {
    private ServiceLoader serviceLoader;

    public void setServiceLoader(ServiceLoader serviceLoader) {
        this.serviceLoader = serviceLoader;
    }

    public Map<String, List> getDetailServices() {
        if (serviceLoader == null)
            serviceLoader = (ServiceLoader) ContainerManager.getComponent("servicesLoader");
        Map<String, List> availableServices = new HashMap<String, List>();
        Map<String, Object> services = serviceLoader.getServices();
        Iterator<Map.Entry<String, Object>> servicesIterator = services.entrySet().iterator();
        while (servicesIterator.hasNext()) {
            Map.Entry<String, Object> service = servicesIterator.next();
            String serviceKey = service.getKey();
            Object serviceObject = service.getValue();
            List<String> exposedMethod = new ArrayList<String>();
            Class[] interfaces = serviceObject.getClass().getInterfaces();
            for (int i = 0; i < interfaces.length; i++) {
                Method[] methods = interfaces[i].getMethods();
                for (int j = 0; j < methods.length; j++) {
                    String methodName = methods[j].getName();
                    Class[] classesType = methods[j].getParameterTypes();
                    methodName = methodName + "(";
                    for (int k = 0; k < classesType.length; k++) {
                        if(k == 0)
                            methodName = methodName + classesType[k].getName();
                        else
                            methodName = methodName + "," + classesType[k].getName();
                    }
                    methodName = methodName + ")";
                    exposedMethod.add(methodName);
                }
            }
            availableServices.put(serviceKey, exposedMethod);
        }
        return availableServices;
    }
}
