package net.customware.webservice.json.service.loader;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.confluence.plugin.descriptor.rpc.XmlRpcModuleDescriptor;
import com.atlassian.confluence.plugin.descriptor.rpc.SoapModuleDescriptor;
import com.atlassian.confluence.plugin.descriptor.rpc.RpcModuleDescriptor;
import bucket.container.ContainerManager;

import java.util.*;

import net.customware.webservice.json.ConstantCollection;
import net.customware.webservice.json.util.ConfigurationUtil;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Apr 13, 2009
 * Time: 7:49:49 AM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Class to load available services. JSON administration page uses this class to load all the existing services
 * for certain type, either XMLRPC or SOAP.
 */
public class ServiceLoader {
    private PluginAccessor pluginAccessor;
    private Map<String, Object> services = new HashMap();
    
    public void setPluginAccessor(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    /**
     * Reset the available services.
     */
    public void resetServices() {
        services = new HashMap<String, Object>();
    }
    /**
     * Initilize services by loading the existing XMLRPC services. The type of services which user loads is stored
     * in table OS_PROPERTYENTRY. 
     */
    public void init()
    {
        int configValue = -1;
        try
        {
            configValue = ConfigurationUtil.getConfiguration();
        }
        catch(Exception e)
        {
            configValue = ConfigurationUtil.saveConfiguration(ConstantCollection.JSON_DEFAULT_SERVICE);
        }
        loadAllServices(configValue);
    }
    /**
     * Load all services which type is either XMLRPC or SOAP. 
     */
    public void loadAllServices(int type) {
        resetServices();
        PluginAccessor pluginAccessor = (PluginAccessor) ContainerManager.getComponent("pluginAccessor");
        List loadedServices;
        if(type == ConstantCollection.XMLRPC_SERVICE)
            loadedServices = pluginAccessor.getEnabledModuleDescriptorsByClass(XmlRpcModuleDescriptor.class);
        else
            loadedServices = pluginAccessor.getEnabledModuleDescriptorsByClass(SoapModuleDescriptor.class);

        for (Iterator it = loadedServices.iterator(); it.hasNext();) {
            RpcModuleDescriptor descriptor = (RpcModuleDescriptor )it.next();
            Object module = descriptor.getModule();
            services.put(descriptor.getKey(), module);
        }
    }
    /**
     * Get service based on the name of the service. The service name is a key of XMLRPC/SOAP plugin descriptor.
     */
    public Object getService(String name)
    {
        Object service = services.get(name);
        return service;
    }
    /**
     * Get all the available services.
     */
    public Map<String, Object> getServices()
    {
        return this.services;
    }
}
