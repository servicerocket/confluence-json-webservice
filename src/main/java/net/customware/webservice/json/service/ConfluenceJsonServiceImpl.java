package net.customware.webservice.json.service;

import net.customware.webservice.json.util.ServiceUtil;

import java.lang.reflect.Method;

import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Mar 23, 2009
 * Time: 7:39:33 AM
 * To change this template use File | Settings | File Templates.
 */
/**
 * This is a proxy class for all of the services inside this plugin, including the existing services 
 */
public class ConfluenceJsonServiceImpl implements ConfluenceJsonService {
    private ServiceUtil serviceUtil;

    public void init() {
        serviceUtil = new ServiceUtil();
    }

    public Object executeMethod(Object service, Method method, Object[] params, String methodName) throws RemoteException {
        Object invokedResult = null;
        if(serviceUtil == null)
            serviceUtil = new ServiceUtil();
        try {
            invokedResult = method.invoke(service, params);
        }
        catch (Exception e) {
            return new RemoteException(e);
        }
        return invokedResult;
    }

}
