package net.customware.webservice.json.service;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Method;

import net.customware.webservice.json.ConstantCollection;
import net.customware.webservice.json.util.JsonUtil;
import com.atlassian.confluence.rpc.auth.TokenAuthenticationManager;
import com.atlassian.confluence.rpc.InvalidSessionException;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import bucket.container.ContainerManager;


/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Apr 6, 2009
 * Time: 7:16:39 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Interceptor of proxy class. Before the real service is executed, it should go to this class to be checked for
 * authorization purpose. 
 */
public class ConfluenceJsonSecurity implements MethodInterceptor {
    private TokenAuthenticationManager tokenAuthenticationManager;
    private UserAccessor userAccessor;
    public Object invoke(MethodInvocation invokedObject) throws Throwable {
        tokenAuthenticationManager = (TokenAuthenticationManager) ContainerManager.getComponent("tokenAuthenticationManager");
        userAccessor = (UserAccessor)ContainerManager.getComponent("userAccessor");

        Method methodInvoked = invokedObject.getMethod();
        Object[] args = invokedObject.getArguments();
        Object[] usernamePassword = (Object[])args[2];
        Object service;

        try {
            if(args[args.length - 1].toString().equals(ConstantCollection.PROXIED_LOGIN)){
//  Get the user with the specified username and password and put the user object to ThreadLocal.               
                User user = userAccessor.getUser(usernamePassword[0].toString());                
                String token = tokenAuthenticationManager.login(usernamePassword[0].toString(), usernamePassword[1].toString());
                if (token != null) {
                    AuthenticatedUserThreadLocal.setUser(user);
                }                
                return token;
            }else if(args[args.length - 1].toString().equals(ConstantCollection.PROXIED_LOGOUT)){
                return tokenAuthenticationManager.logout(args[0].toString());
            } else {
                try {
// Get the user from thread local. if user does not exist, get from tokenAuthenticationManager
                    if (usernamePassword.length > 0) {
                        String token = usernamePassword[0].toString();
                        User user = AuthenticatedUserThreadLocal.getUser();
                        try
                        {
                            if (StringUtils.isNotBlank(token)) {
                                user = tokenAuthenticationManager.makeNonAnonymousUserFromToken(token);
                            }
                            AuthenticatedUserThreadLocal.setUser(user);
                        }
                        catch(InvalidSessionException e){
                        }
                    }
                    service = invokedObject.getThis();

                    Object returnValue = methodInvoked.invoke(service, args);
                    return returnValue;
                }
                catch (Exception e) {
                    return e;
                }
            }

        }
        catch (Exception e) {
            return e;
        }        
    }

    public void setTokenAuthenticationManager(TokenAuthenticationManager tokenAuthenticationManager) {
        this.tokenAuthenticationManager = tokenAuthenticationManager;
    }
}
