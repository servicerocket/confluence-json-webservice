package net.customware.webservice.json.gui;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import bucket.container.ContainerManager;
import net.customware.webservice.json.service.loader.ServiceLoader;
import net.customware.webservice.json.ConstantCollection;
import net.customware.webservice.json.util.ConfigurationUtil;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Apr 19, 2009
 * Time: 12:09:21 AM
 * To change this template use File | Settings | File Templates.
 */
/**
 * A webwork action for JSON administration. This page is to configure which service that you want to load.
 * This plugin only loads either XMLRPC or SOAP service. The action uses JSON to display the list of available
 * services in Confluence.
 */
public class JsonConfigurationPageAction extends ConfluenceActionSupport {
    public String execute() throws Exception {
        message = "";
        ServiceLoader serviceLoader = (ServiceLoader) ContainerManager.getComponent("servicesLoader");
        if (serviceOption != null) {            
            if (serviceOption.equals("" + ConstantCollection.XMLRPC_SERVICE)) {
                serviceLoader.loadAllServices(ConstantCollection.XMLRPC_SERVICE);
                ConfigurationUtil.updateConfiguration(ConstantCollection.XMLRPC_SERVICE);
                message = "All XMLRPC services have been loaded";
            } else if(serviceOption.equals(""+ConstantCollection.SOAP_SERVICE)){
                serviceLoader.loadAllServices(ConstantCollection.SOAP_SERVICE);
                ConfigurationUtil.updateConfiguration(ConstantCollection.SOAP_SERVICE);
                message = "All SOAP services have been loaded";
            }
            else{
                message = "Service/s to load is not available";
            }
        }
        else
        {
            int serviceType = ConfigurationUtil.getConfiguration();
            serviceOption = serviceType + "";
            serviceLoader.loadAllServices(serviceType);
        }
        return SUCCESS;
    }

    private String message;
    private String serviceOption;

    public void setServiceOption(String serviceOption) {
        this.serviceOption = serviceOption;
    }

    public String getServiceOption() {
        return this.serviceOption;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
