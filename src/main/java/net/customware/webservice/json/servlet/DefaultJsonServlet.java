package net.customware.webservice.json.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.customware.webservice.json.util.HttpJsonUtil;
import net.customware.webservice.json.util.ServiceUtil;
import net.customware.webservice.json.service.loader.ServiceLoader;
import net.customware.webservice.json.ConstantCollection;

import java.util.List;
import java.util.Map;
import java.lang.reflect.Method;

import bucket.container.ContainerManager;
import com.atlassian.confluence.rpc.RemoteException;

/**
 * This class is a servlet to serve all the json rpc request
 */
public class DefaultJsonServlet extends HttpServlet{

    private Logger logger = LoggerFactory.getLogger(DefaultJsonServlet.class);
    private ServiceUtil serviceUtil = new ServiceUtil();
    private HttpJsonUtil httpUtil;

    public void service(HttpServletRequest request, HttpServletResponse response)
    {
        if(httpUtil == null) httpUtil = new HttpJsonUtil();
        try {
            initializeDefaultService();
            request.setCharacterEncoding("UTF-8");
            response.setContentType("text/plain");
            String serviceName = httpUtil.getServiceName(request);
            String methodName = httpUtil.getMethodName(request);
            List<String> parameter = httpUtil.getParameters(request);
            Object service = serviceUtil.getService(serviceName);
            if (service == null) {
                Exception exception = new RemoteException(ConstantCollection.EXCEPTION_SERVICE_UNAVAILABLE);
                throw exception;
            }

            Map<Method, Object[]> methodnParameter = serviceUtil.matchMethod(service, methodName, parameter);
            if(methodnParameter.size() == 0){
                Exception exception = new RemoteException(ConstantCollection.EXCEPTION_METHOD_UNAVAILABLE);
                throw exception;
            }
            String result = serviceUtil.invokeAllServices(service, methodnParameter);
            httpUtil.sendToClient(response, request, result);
        }
        catch (Exception e) {
            e.printStackTrace();
            httpUtil.sendToClient(response, request, e.getMessage());
        }
    }
    /**
     * To initialize the service. By default, it will load all the xmlrpc services available in Confluence
     */
    public void initializeDefaultService() throws Exception
    {
        ServiceLoader serviceLoader = (ServiceLoader)ContainerManager.getComponent("servicesLoader");
        if(serviceLoader.getServices().size() == 0)
        {
            serviceLoader.init();
        }
    }
    
    public void setRequestUtil(HttpJsonUtil httpUtil)
    {
        this.httpUtil = httpUtil;
    }
    public HttpJsonUtil getRequestUtil()
    {
        return this.httpUtil;
    }


}
