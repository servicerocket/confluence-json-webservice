package net.customware.webservice.json.service;

import junit.framework.TestSuite;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Mar 28, 2009
 * Time: 2:46:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class DummyData {
    private String name;
    private Integer age;
    private String description;
    private DummyDataPhone phones;

    public void setDescription(String description)
    {
        this.description = description;
    }
    public String getDescription()
    {
        return this.description;
    }
    public void setPhones(DummyDataPhone phones)
    {
        this.phones = phones;
    }
    public DummyDataPhone getPhones()
    {
        return this.phones;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void setAge(Integer age){
        this.age = age;
    }
    public String getName()
    {
        return this.name;
    }
    public Integer getAge(){
        return this.age;
    }

    public String print10Times(String name)
    {
        String tempName = "";
        if(name == null)
            tempName = this.name;
        else
            tempName = name;
        String returnName = "";
        for(int i=0; i<10; i++)
        {
            returnName += tempName;
        }
        return returnName;
    }

    
}
