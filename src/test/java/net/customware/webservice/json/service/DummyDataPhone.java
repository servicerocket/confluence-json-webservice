package net.customware.webservice.json.service;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: May 2, 2009
 * Time: 10:51:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class DummyDataPhone {
    private Integer workPhone;
    private Integer homePhone;

    public void setWorkPhone(Integer workPhone)
    {
        this.workPhone = workPhone;
    }
    public Integer getWorkPhone(){
        return this.workPhone;
    }
    public void setHomePhone(Integer homePhone)
    {
        this.homePhone = homePhone;
    }
    public Integer getHomePhone()
    {
        return this.homePhone;
    }
}
