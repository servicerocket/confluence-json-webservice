package net.customware.webservice.json.service;

public class DummyService {

    public String getMyName()
    {
        return "my name is arie murdianto";
    }
    public String whoAmI(String name, Integer age)
    {
        return "I am "+name+" and my age: "+age;
    }
    public String getAbitDetail(String name, Integer age)
    {
        return "I am "+name+" and my age: "+age;
    }
    public String getAbitDetail(String name, String agess)
    {
        Integer.parseInt("&&");
        return "";
    }
    public String getMyDetail(DummyData data)
    {
        return "my name is: "+data.getName() +" and I am "+data.getAge();
    }
    public DummyData getDetailOfMine(String name, Integer age)
    {
        DummyData dummyData = new DummyData();
        dummyData.setName(name);
        dummyData.setAge(age);
        return dummyData;
    }
}
