package net.customware.webservice.json.util;

import org.junit.Test;

import junit.framework.Assert;
import junit.framework.TestSuite;
import net.customware.webservice.json.service.DummyData;
import net.customware.webservice.json.service.DummyDataPhone;


public class JsonUtilTest {
//	@Test
//	public void testConvertObjectToJson() {
//        DummyData dummyData = new DummyData();
//        dummyData.setAge(12);
//        dummyData.setName("john doe");
//        dummyData.setDescription("my description");
//        DummyDataPhone phone = new DummyDataPhone();
//        phone.setHomePhone(new Integer(54335664));
//        phone.setWorkPhone(new Integer(54335666));
//        dummyData.setPhones(phone);
//        String convertedObject = JsonUtil.convertObjectToJson(dummyData);
//        System.out.println("-> "+convertedObject);
//        Assert.assertEquals("{\"age\":12,\"description\":\"my description\",\"name\":\"john doe\",\"phones\":{\"workPhone\":54335666,\"homePhone\":54335664}}", convertedObject);
//    }
    @Test
    public void testConvertJsonStringToObject()
    {
        DummyData dummyData = new DummyData();
        dummyData.setName("john doe");
        dummyData.setAge(new Integer(12));
        dummyData.setDescription("my description");
        String convertedObject = JsonUtil.convertObjectToJson(dummyData);
        DummyData deconvertedObject =(DummyData)JsonUtil.convertJsonStringToObject(convertedObject, DummyData.class);

        Assert.assertEquals(deconvertedObject.getClass().getName(),DummyData.class.getName());
    }
}
