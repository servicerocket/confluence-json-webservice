package net.customware.webservice.json.util;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.Before;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import net.customware.webservice.json.service.DummyService;

public class ServiceUtilTest {

    private Mockery mockery = new JUnit4Mockery();
    private HttpServletRequest request;

    @Before
    public void setUpMock()
    {
        request = mockery.mock(HttpServletRequest.class);
    }

// Test method without parameter and unique   
	@Test        
	public void testMatchMethod()
	{
        ServiceUtil serviceUtil = new ServiceUtil();
        DummyService dummyService = new DummyService();
        List<String> parameters = new ArrayList();
        parameters.add("123");
        parameters.add("123");

        Map<Method, Object[]> paramMethod = serviceUtil.matchMethod(dummyService, "getAbitDetail", parameters);
        
		Assert.assertEquals(paramMethod.size(), 2);
	}

    public void testInvokeAllServices()
    {
        ServiceUtil serviceUtil = new ServiceUtil();
        DummyService dummyService = new DummyService();
        List<String> parameters = new ArrayList();
        parameters.add("arie");
        parameters.add("123");
        Map<Method, Object[]> paramMethod = serviceUtil.matchMethod(dummyService, "getAbitDetail", parameters);
//        String result = serviceUtil.invokeAllServices(dummyService, paramMethod);
//        System.out.println(result);
    }
// Test method with parameter and there is an overlap method exist
    @Test
    public void testLocalizeParameter()
    {
        ServiceUtil serviceUtil = new ServiceUtil();
        DummyService dummyService = new DummyService();
//        Method method = serviceUtil.getMethodBasedOnName(dummyService, "getMyDetail");
//
////        Object[] localizedParameter = serviceUtil.localizeParameter(method, dummyService);
//
//        Assert.assertEquals(method.getName(), "whoAmI");
    }
}
