package net.customware.webservice.json.util;

import org.jmock.Mockery;
import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import javax.servlet.http.HttpServletRequest;

import net.customware.webservice.json.ConstantCollection;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: Mar 26, 2009
 * Time: 10:32:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class HttpJsonUtilTest {
    private HttpServletRequest request;
    private Mockery mockery;

    @Before
    public void setUpServletRequest()
    {
        mockery = new JUnit4Mockery();
        request = mockery.mock(HttpServletRequest.class);
    }

    @Test
    public void testGetParameters()
    {
        HttpJsonUtil reqUtil = new HttpJsonUtil();
        final String parameterTest = "{'name':'arie','ganteng':'{'tergantung':'cakep','tergantung':'nggak'}'}";
        mockery.checking(new Expectations(){
            {
                oneOf(request).getParameter("parameters");
                will(returnValue("thisIsToken;"+parameterTest));
            }
        });
        List<String> parameters = reqUtil.getParameters(request);
        Assert.assertEquals(parameters.size(), 2);
        String secondParameter = parameters.get(1);
        String endResult = "{'name':'arie','ganteng':'"+ConstantCollection.MACRO_CHAR_OPEN_ESCAPE+"'tergantung':'cakep','tergantung':'nggak'"+ConstantCollection.MACRO_CHAR_CLOSE_ESCAPE+"'}";
        Assert.assertEquals(endResult, secondParameter);
    }
    
    @Test
    public void testGetMethodNameAndServiceName()
    {
        HttpJsonUtil reqUtil = new HttpJsonUtil();
        mockery.checking(new Expectations() {
            {
                oneOf(request).getPathInfo();
                will(returnValue(ConstantCollection.PATH_INFO +"confluence1/getMyName"));
            }
        }
        );
        String serviceMethodName = reqUtil.fetchServiceMethodName(request);
        Assert.assertEquals("confluence1/getMyName", serviceMethodName);

        mockery.checking(new Expectations() {
            {
                oneOf(request).getPathInfo();
                will(returnValue(ConstantCollection.PATH_INFO +"confluence1/getMyName"));
            }
        }
        );
        String methodName = reqUtil.getMethodName(request);
        Assert.assertEquals("getMyName", methodName);

        mockery.checking(new Expectations() {
            {
                oneOf(request).getPathInfo();
                will(returnValue(ConstantCollection.PATH_INFO +"confluence1/getMyName"));
            }
        }
        );
        String serviceName = reqUtil.getServiceName(request);
        Assert.assertEquals("confluence1", serviceName);
    }
}
