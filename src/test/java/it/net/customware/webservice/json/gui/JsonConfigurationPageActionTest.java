package it.net.customware.webservice.json.gui;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import net.customware.webservice.json.ConstantCollection;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: May 31, 2009
 * Time: 9:25:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class JsonConfigurationPageActionTest extends AbstractConfluencePluginWebTestCase {
    protected ConfluenceWebTester confluenceWebTester;
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        confluenceWebTester = this.getConfluenceWebTester();
    }
    public void testJSONPluginInstalled()
    {
        this.gotoPage("/admin/plugins.action");
        this.assertTextPresent("Confluence JSON WebService");
    }
    public void testLoadDefaultServices()
    {
        this.gotoRootWindow();
        this.gotoPage("/admin/JsonConfigurationPage.action");
        this.assertTextPresent("Service To Expose");

        this.clickButton("showService");
    }
    public void testLoadSoapServices()
    {
        this.gotoRootWindow();
        this.gotoPage("/admin/JsonConfigurationPage.action");
        this.clickRadioOption("serviceOption", ConstantCollection.SOAP_SERVICE+"");
        this.submit("loadService");
        this.assertTextPresent("All SOAP services have been loaded");
    }
    public void testLoadXMLRPCServices()
    {
        this.gotoPage("/admin/JsonConfigurationPage.action");
        this.clickRadioOption("serviceOption", ConstantCollection.XMLRPC_SERVICE+"");
        this.submit("loadService");
        this.assertTextPresent("All XMLRPC services have been loaded");
    }
}