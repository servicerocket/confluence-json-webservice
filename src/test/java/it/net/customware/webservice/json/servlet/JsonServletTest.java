package it.net.customware.webservice.json.servlet;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import net.customware.webservice.json.Constant;
import org.junit.Test;
import org.junit.Assert;

/**
 * Created by IntelliJ IDEA.
 * User: arie
 * Date: 27/05/2009
 * Time: 11:41:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class JsonServletTest extends AbstractConfluencePluginWebTestCase{

    protected ConfluenceWebTester confluenceWebTester;
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        confluenceWebTester = this.getConfluenceWebTester();
//        confluenceWebTester.
    }
    public String getUrlAddress(String method)
    {
        return "/plugins/servlet/jsonService/confluence-xmlrpc/"+method+"?parameters=";
    }

    public void testLogin()
    {
        String username = Constant.ADMIN_USERNAME;
        String password = Constant.ADMIN_PASSWORD;
        String method = "login";
        this.gotoPage(getUrlAddress(method)+username+";"+password);
        this.assertTextNotPresent("no user could be found with this username");
        this.assertTextNotPresent("failed - incorrect password");
    }

    public void testLoginUsernameWrong()
    {
        String username = Constant.ADMIN_USERNAME;
        String password = Constant.ADMIN_PASSWORD;
        String method = "login";
        this.gotoPage(getUrlAddress(method)+username+";"+password);
        
        this.assertTextNotPresent("no user could be found with this username");
    }

    public void testLoginPasswordWrong()
    {
        String username = Constant.ADMIN_USERNAME;
        String password = Constant.ADMIN_PASSWORD;
        String method = "login";
        this.gotoPage(getUrlAddress(method)+username+";"+password);

        this.assertTextNotPresent("failed - incorrect password");
    }

    public void testCreatePage()
    {
        String username = Constant.ADMIN_USERNAME;
        String password = Constant.ADMIN_PASSWORD;
        String method = "login";
        this.gotoPage(getUrlAddress(method)+username+";"+password+"&callback=noLabel");
        String jsonResponse = confluenceWebTester.getPageSource();
        jsonResponse = jsonResponse.replaceAll("\"","");
        method="storePage";
        String page = "{'title':'this is the test page','content':'{pagetree:root=Home}','space':'ds','url':'http://localhost:1990/confluence/display/ds/this+is+the+test+page'}";

        this.gotoPage(getUrlAddress(method)+jsonResponse+";"+page);
        this.assertTextPresent("this is the test page");
    }

    public void testGetSystemInfo()
    {
        String username = Constant.ADMIN_USERNAME;
        String password = Constant.ADMIN_PASSWORD;
        String method = "login";
        this.gotoPage(getUrlAddress(method) + username + ";" + password + "&callback=noLabel");
        String jsonResponse = confluenceWebTester.getPageSource();
        jsonResponse = jsonResponse.replaceAll("\"", "");
        method = "getServerInfo";

        this.gotoPage(getUrlAddress(method) + jsonResponse);

        this.assertTextPresent("1518");
        this.assertTextPresent("buildId");
    }

    public void testGetPage()
    {
        String username = Constant.ADMIN_USERNAME;
        String password = Constant.ADMIN_PASSWORD;
        String method = "login";
        this.gotoPage(getUrlAddress(method)+username+";"+password+ "&callback=noLabel");
        String jsonResponse = confluenceWebTester.getPageSource();
        jsonResponse = jsonResponse.replaceAll("\"", "");
        
        method="getPage";
        String page = "Creating a task list";
        String spaceKey = "ds";
        this.gotoPage(getUrlAddress(method)+jsonResponse+";"+spaceKey+";"+page);
        this.assertTextPresent("Creating a task list");
    }

    public void testGetPagesInSpace()
    {
        String username = Constant.ADMIN_USERNAME;
        String password = Constant.ADMIN_PASSWORD;
        String method = "login";
        String url = getUrlAddress(method)+username+";"+password+ "&callback=noLabel";
        this.gotoPage(url);
        String jsonResponse = confluenceWebTester.getPageSource();
        jsonResponse = jsonResponse.replaceAll("\"", "");
        
        method="getPages";
        String spaceKey = "ds";
        this.gotoPage(getUrlAddress(method)+jsonResponse+";"+spaceKey);

        this.assertTextPresent("ds");
    }

    public void testSearch()
    {
        String username = Constant.ADMIN_USERNAME;
        String password = Constant.ADMIN_PASSWORD;
        String method = "login";
        this.gotoPage(getUrlAddress(method)+username+";"+password+ "&callback=noLabel");
        String jsonResponse = confluenceWebTester.getPageSource();
        jsonResponse = jsonResponse.replaceAll("\"", "");

        method="search";
        String query = "test page";
        String maxResult = "1000";
        this.gotoPage(getUrlAddress(method)+jsonResponse+";"+query+";"+maxResult);

        this.assertTextNotPresent("error");
    }

//    public void testBloggerXmlRpcUserInfo()
//    {
//        String targetUrl = "plugins/servlet/jsonService/blogger-xmlrpc/getUserInfo?parameters=confluence;"+Constant.ADMIN_USERNAME +
//                ";"+Constant.ADMIN_PASSWORD;
//        this.gotoPage(targetUrl);
//        this.assertTextPresent(Constant.ADMIN_USERNAME);
//    }
}
